package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 */
public class OrderReceipt {
    private Order order;

    public OrderReceipt(Order order) {
        this.order = order;
    }

    public String printReceipt() {
        StringBuilder output = new StringBuilder();

        output.append("======Printing Orders======").append("\n");

        output.append(order.getCustomerName());
        output.append(order.getAddress());

        double totalSalesTax = 0d;
        double totalOrderAmount = 0d;
        for (LineItem lineItem : order.getLineItems()) {
            output.append(lineItem.appendLineItem());

            double salesTax = lineItem.calculateSalesTax();
            totalSalesTax += salesTax;

            totalOrderAmount += lineItem.totalAmount() + salesTax;
        }

        output.append("Sales Tax").append('\t').append(totalSalesTax);

        output.append("Total Amount").append('\t').append(totalOrderAmount);
        return output.toString();
    }

}