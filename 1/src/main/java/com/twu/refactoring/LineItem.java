package com.twu.refactoring;

public class LineItem {
	private String description;
	private double price;
	private int quantity;
	private double taxRate;

	public LineItem(String description, double price, int quantity) {
		super();
		this.description = description;
		this.price = price;
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public double getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

    double totalAmount() {
        return price * quantity;
    }

	public StringBuilder appendLineItem() {
		StringBuilder output = new StringBuilder();
		output.append(getDescription()).append('\t')
				.append(getPrice()).append('\t')
				.append(getQuantity()).append('\t')
				.append(totalAmount()).append('\n');
		return output;
	}

	double calculateSalesTax() {
		taxRate = .10;
		return totalAmount() * taxRate;
	}
}