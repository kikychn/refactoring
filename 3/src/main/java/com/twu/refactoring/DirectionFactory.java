package com.twu.refactoring;

public class DirectionFactory {
    public static Direction createDirection(char direction) {
        if (direction == 'N') {
            return new North();
        }
        if (direction == 'S') {
            return new South();
        }
        if (direction == 'E') {
            return new East();
        }
        return new West();
    }
}
