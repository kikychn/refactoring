package com.twu.refactoring;

public enum DateEnum {

    Year("Year", 0, 4, 2000, 2012, 4),
    Month("Month", 5, 7, 1, 12, 2),
    Day("Date", 8, 10, 1, 31, 2),
    Hour("Hour", 11, 13, 0, 23, 2),
    Minute("Minute", 14, 16, 0, 59, 2);

    private String dateType;
    private int startIndex;
    private int endIndex;
    private int lowerLimit;
    private int upperLimit;
    private int characterCount;

    DateEnum(String dateType, int startIndex, int endIndex, int lowerLimit, int upperLimit, int characterCount) {
        this.dateType = dateType;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.characterCount = characterCount;
    }

    public int get(String dateAndTimeString) {
        int dateTime;
        try {
            String minuteString = dateAndTimeString.substring(startIndex, endIndex);
            dateTime = Integer.parseInt(minuteString);
        } catch (StringIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(String.format("%s string is less than %d characters", dateType, characterCount));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(String.format("%s is not an integer", dateType));
        }
        if (dateTime < lowerLimit || dateTime > upperLimit)
            throw new IllegalArgumentException(String.format("%s cannot be less than %d or more than %d", dateType, lowerLimit, upperLimit));
        return dateTime;
    }
}