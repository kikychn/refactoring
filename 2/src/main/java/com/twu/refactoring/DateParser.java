package com.twu.refactoring;

import java.util.*;

public class DateParser {
    private final String dateAndTimeString;
    private static final Map<String, TimeZone> KNOWN_TIME_ZONES;
    DateEnum dateEnum;

    static {
        KNOWN_TIME_ZONES = new HashMap<>();
        KNOWN_TIME_ZONES.put("UTC", TimeZone.getTimeZone("UTC"));
    }

    /**
     * Takes a date in ISO 8601 format and returns a date
     *
     * @param dateAndTimeString - should be in format ISO 8601 format
     *                          examples -
     *                          2012-06-17 is 17th June 2012 - 00:00 in UTC TimeZone
     *                          2012-06-17TZ is 17th June 2012 - 00:00 in UTC TimeZone
     *                          2012-06-17T15:00Z is 17th June 2012 - 15:00 in UTC TimeZone
     */
    public DateParser(String dateAndTimeString) {
        this.dateAndTimeString = dateAndTimeString;
    }

    public Date parse() {
        int year, month, date, hour, minute;
        year = dateEnum.Year.get(dateAndTimeString);
        month = dateEnum.Month.get(dateAndTimeString);
        date = dateEnum.Day.get(dateAndTimeString);

        if (dateAndTimeString.substring(11, 12).equals("Z")) {
            hour = 0;
            minute = 0;
        } else {
            hour = dateEnum.Hour.get(dateAndTimeString);
            minute = dateEnum.Minute.get(dateAndTimeString);
        }

        Calendar calendar = getCalendar(year, month, date, hour, minute);
        return calendar.getTime();
    }

    private Calendar getCalendar(int year, int month, int date, int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(KNOWN_TIME_ZONES.get("UTC"));
        calendar.set(year, month - 1, date, hour, minute, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

}
