package com.twu.refactoring;

public class NumberCruncher {
    private final int[] numbers;
    private int countPositive = 0;
    private int countEven = 0;

    public NumberCruncher(int... numbers) {
        this.numbers = numbers;
    }

    public int countEven() {
        for (int number : numbers) {
            if (number % 2 == 0) countEven++;
        }
        return countEven;
    }

    public int countOdd() {
        countEven();
        return numbers.length - countEven;
    }

    public int countPositive() {
        for (int number : numbers) {
            if (number >= 0) countPositive++;
        }
        return countPositive;
    }

    public int countNegative() {
        countPositive();
        return numbers.length - countPositive;
    }
}
