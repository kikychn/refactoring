package com.twu.refactoring;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Customer {

    private String name;
    private List<Rental> rentalList = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental rental) {
        rentalList.add(rental);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        Iterator<Rental> rentals = rentalList.iterator();
        StringBuilder result = new StringBuilder("Rental Record for " + getName() + "\n");
        while (rentals.hasNext()) {
            Rental rental = rentals.next();
            double thisAmount = calculateMovieAmount(rental);
            frequentRenterPoints += getFrequentRenterPoints(rental);
            result.append("\t").append(rental.getMovie().getTitle()).append("\t").append(String.valueOf(thisAmount)).append("\n");
            totalAmount += thisAmount;

        }
        result.append("Amount owed is ").append(String.valueOf(totalAmount)).append("\n");
        result.append("You earned ").append(String.valueOf(frequentRenterPoints)).append(" frequent renter points");
        return result.toString();
    }

    private double calculateMovieAmount(Rental rental) {
        Movie movie = new Movie();
        if (rental.getMovie().getPriceCode() == Movie.REGULAR) {
            movie = new Regular();
        }
        if (rental.getMovie().getPriceCode() == Movie.NEW_RELEASE) {
            movie = new NewRelease();
        }
        if (rental.getMovie().getPriceCode() == Movie.CHILDRENS) {
            movie = new Childrens();
        }
        return movie.calculateAmount(rental);
    }

    private int getFrequentRenterPoints(Rental rental) {
        if ((rental.getMovie().getPriceCode() == Movie.NEW_RELEASE)
                && rental.getDaysRented() > 1)
            return 2;
        return 1;
    }

}
