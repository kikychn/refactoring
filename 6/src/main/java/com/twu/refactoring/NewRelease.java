package com.twu.refactoring;

public class NewRelease extends Movie {
    public NewRelease() {
    }

    public NewRelease(String title, int priceCode) {
        super(title, priceCode);
    }

    @Override
    public double calculateAmount(Rental rental) {
        double amount = rental.getDaysRented() * 3;
        return amount;
    }
}
