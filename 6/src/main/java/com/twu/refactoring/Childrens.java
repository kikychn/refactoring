package com.twu.refactoring;

public class Childrens extends Movie {
    public Childrens() {
    }

    public Childrens(String title, int priceCode) {
        super(title, priceCode);
    }

    @Override
    public double calculateAmount(Rental rental) {
        double amount = 1.5;
        if (rental.getDaysRented() > 3)
            amount += (rental.getDaysRented() - 3) * 1.5;
        return amount;
    }
}
